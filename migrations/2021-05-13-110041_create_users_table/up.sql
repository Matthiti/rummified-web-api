CREATE TABLE `users` (
    `id` BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `uuid` VARCHAR(36) UNIQUE NOT NULL,
    `external_id` BIGINT UNSIGNED UNIQUE NOT NULL,
    `credentials` VARCHAR(255) NOT NULL,

    INDEX(`uuid`),
    INDEX(`external_id`)
)
