# Rummified Web API

TODO

## Dev

- Manage database migrations: 
  
    - Install: 
        ```shell
        cargo install diesel_cli --no-default-features --features mysql
        ```
    - Generate migration:
        ```shell
        diesel migration generate create_users_table
        ```
    - Apply migration(s):
        ```shell
        diesel migration run --database-url=mysql://root:root@127.0.0.1/rummified-web-api 
        ```
    - Generate schema:
        ```shell
        diesel print-schema --database-url=mysql://root:root@127.0.0.1/rummified-web-api > src/schema.rs
        ```