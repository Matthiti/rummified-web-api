use crate::db::schema::*;
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Queryable, Identifiable)]
pub struct User {
    pub id: u64,
    pub uuid: String,
    pub external_id: u64,
    pub credentials: String
}

#[derive(Insertable, Debug)]
#[table_name = "users"]
pub struct NewUser {
    pub uuid: String,
    pub external_id: u64,
    pub credentials: String
}