use crate::models::user::User;

pub struct AuthUser {
    pub user: User,
    pub secret_key: SecretKey
}

pub struct SecretKey {
    pub key: String,
    pub nonce: String
}

impl AuthUser {
    pub fn new(user: User, key: String, nonce: String) -> Self {
        AuthUser {
            user,
            secret_key: SecretKey {
                key,
                nonce
            }
        }
    }
}