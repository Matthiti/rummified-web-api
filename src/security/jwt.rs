use serde::{Deserialize, Serialize};
use chrono::{Utc, Duration};
use jsonwebtoken::{encode, decode, Header, EncodingKey, errors::Error, DecodingKey, Validation};

use crate::config;

#[derive(Debug, Serialize, Deserialize)]
pub struct Claims {
    pub sub: String,
    pub secret_key: KeyClaims,
    pub iat: usize,
    pub exp: usize
}

#[derive(Debug, Serialize, Deserialize)]
pub struct KeyClaims {
    pub key: String,
    pub nonce: String
}

pub fn create_token(identifier: &str, key: &str, nonce: &str) -> Result<String, Error> {
    let claims = Claims {
        sub: identifier.to_string(),
        secret_key: KeyClaims {
            key: key.to_string(),
            nonce: nonce.to_string()
        },
        iat: Utc::now().timestamp() as usize,
        exp: (Utc::now() + Duration::minutes(30)).timestamp() as usize
    };

    encode(&Header::default(), &claims, &EncodingKey::from_secret((*config::JWT_SECRET).as_bytes()))
}

pub fn validate_token(token: &str) -> Result<Claims, Error> {
    let decoded = decode(token, &DecodingKey::from_secret((*config::JWT_SECRET).as_bytes()), &Validation::default())?;
    Ok(decoded.claims)
}
