use actix_web::{ResponseError, HttpResponse};
use derive_more::Display;

use crate::response;
use actix_web::http::StatusCode;

#[derive(Debug, Display)]
pub enum ServiceError {
    #[display(fmt = "Bad Request")]
    BadRequest(String),

    #[display(fmt = "Unauthorized")]
    Unauthorized(String),

    #[display(fmt = "Internal Server Error")]
    InternalServerError(String),

    #[display(fmt = "Rummified Error")]
    RummifiedError(StatusCode, String)
}

impl ResponseError for ServiceError {
    fn error_response(&self) -> HttpResponse {
        match self {
            ServiceError::BadRequest(msg) => {
                HttpResponse::BadRequest()
                    .json(response::ErrorResponse::new(msg.clone()))
            },
            ServiceError::Unauthorized(msg) => {
                HttpResponse::Unauthorized()
                    .json(response::ErrorResponse::new(msg.clone()))
            },
            ServiceError::InternalServerError(msg) => {
                HttpResponse::InternalServerError()
                    .json(response::ErrorResponse::new(msg.clone()))
            },
            ServiceError::RummifiedError(status_code, msg) => {
                HttpResponse::build(*status_code)
                    .json(response::ErrorResponse::new(msg.clone()))
            }
        }
    }
}
