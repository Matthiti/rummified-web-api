#[macro_use]
extern crate lazy_static;

#[macro_use]
extern crate diesel;

use actix_web::{App, HttpServer};
use dotenv::dotenv;
use diesel::prelude::*;
use diesel::r2d2::{self, ConnectionManager};

mod services;
mod config;
mod response;
mod security;
mod models;
mod db;
mod middleware;
mod errors;

pub type Pool = r2d2::Pool<ConnectionManager<MysqlConnection>>;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    dotenv().ok();

    let db_pool: Pool = r2d2::Pool::builder()
        .build(ConnectionManager::<MysqlConnection>::new(&*config::DATABASE_URL))
        .expect("Failed to create pool.");

    HttpServer::new(move || {
        App::new()
            .data(db_pool.clone())
            .wrap(middleware::jwt::CheckJwt)
            .service(services::auth::login)
            .service(services::proxy::proxy)
    })
    .bind("127.0.0.1:7000")?
    .run()
    .await
}
