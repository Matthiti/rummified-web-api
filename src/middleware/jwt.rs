use actix_web::dev::{Transform, Service, ServiceRequest, ServiceResponse};
use actix_web::{Error, HttpResponse, HttpMessage};
use std::pin::Pin;
use std::task::{Context, Poll};
use futures::future::{Ready, ok};
use futures::Future;
use actix_web::http::Method;
use diesel::prelude::*;

use crate::{response, security, Pool};
use crate::db::schema::users::dsl as schema;
use crate::models::user::User;
use crate::security::auth_user::AuthUser;

lazy_static! {
    pub static ref NO_AUTH_ROUTES: Vec<&'static str> = vec!["/api/auth/login"];
}

pub struct CheckJwt;

impl<S, B> Transform<S> for CheckJwt
    where
        S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = Error>,
        S::Future: 'static,
        B: 'static
{
    type Request = ServiceRequest;
    type Response = ServiceResponse<B>;
    type Error = Error;
    type Transform = CheckJwtMiddleware<S>;
    type InitError = ();
    type Future = Ready<Result<Self::Transform, Self::InitError>>;

    fn new_transform(&self, service: S) -> Self::Future {
        ok(CheckJwtMiddleware { service })
    }
}

pub struct CheckJwtMiddleware<S> {
    service: S
}

impl<S, B> Service for CheckJwtMiddleware<S>
    where
        S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = Error>,
        S::Future: 'static,
        B: 'static,
{
    type Request = ServiceRequest;
    type Response = ServiceResponse<B>;
    type Error = Error;
    type Future = Pin<Box<dyn Future<Output = Result<Self::Response, Self::Error>>>>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.service.poll_ready(cx)
    }

    fn call(&mut self, req: ServiceRequest) -> Self::Future {
        if *req.method() == Method::OPTIONS || (*NO_AUTH_ROUTES).contains(&req.path()) {
            let fut = self.service.call(req);

            return Box::pin(async move {
                fut.await
            });
        }

        let auth_header = req.headers().get("X-Authorization");
        if auth_header.is_none() {
            return Box::pin(async move {
                Ok(
                    req.into_response(
                        HttpResponse::Unauthorized()
                                .json(response::ErrorResponse::new(String::from("Missing 'X-Authorization' header")))
                                .into_body()
                    )
                )
            });
        }

        let auth_header = auth_header.unwrap().to_str();
        if auth_header.is_err() {
            return Box::pin(async move {
                Ok(
                    req.into_response(
                        HttpResponse::Unauthorized()
                                .json(response::ErrorResponse::new(String::from("Invalid 'X-Authorization' header")))
                                .into_body()
                    )
                )
            });
        }

        let auth_header = auth_header.unwrap();
        if !auth_header.starts_with("Bearer ") {
            return Box::pin(async move {
                Ok(
                    req.into_response(
                        HttpResponse::Unauthorized()
                                .json(response::ErrorResponse::new(String::from("Invalid 'X-Authorization' header")))
                                .into_body()
                    )
                )
            });
        }

        let token = &auth_header["Bearer ".len()..];
        return match security::jwt::validate_token(token) {
            Ok(claims) => {
                let conn = req
                    .app_data::<actix_web::web::Data<Pool>>()
                    .unwrap()
                    .get()
                    .unwrap();

                match schema::users.filter(schema::uuid.eq(claims.sub)).get_result::<User>(&conn) {
                    Ok(user) => {
                        let auth_user = AuthUser::new(user, claims.secret_key.key, claims.secret_key.nonce);
                        req.extensions_mut().insert(auth_user);

                        let fut = self.service.call(req);

                        Box::pin(async move {
                            fut.await
                        })
                    },
                    Err(_) => {
                        Box::pin(async move {
                            Ok(
                                req.into_response(
                                    HttpResponse::Unauthorized()
                                        .json(response::ErrorResponse::new(String::from("Invalid token")))
                                        .into_body()
                                )
                            )
                        })
                    }
                }
            },
            Err(_) => {
                Box::pin(async move {
                    Ok(
                        req.into_response(
                            HttpResponse::Unauthorized()
                                    .json(response::ErrorResponse::new(String::from("Invalid token")))
                                    .into_body()
                        )
                    )
                })
            }
        }
    }
}