use serde::Serialize;

#[derive(Serialize)]
pub struct SuccessResponse<T> {
    success: bool,
    data: T
}

impl<T> SuccessResponse<T> {
    pub fn new(data: T) -> Self {
        SuccessResponse {
            success: true,
            data
        }
    }
}

#[derive(Serialize)]
pub struct ErrorResponse {
    success: bool,
    message: String
}

impl ErrorResponse {
    pub fn new(message: String) -> Self {
        ErrorResponse {
            success: false,
            message
        }
    }
}
