table! {
    users (id) {
        id -> Unsigned<Bigint>,
        uuid -> Varchar,
        external_id -> Unsigned<Bigint>,
        credentials -> Varchar,
    }
}
