use std::env;

lazy_static! {
    pub static ref RUMMIFIED_API_BASE_URL: String = String::from("https://www.rummified.com/api");
    pub static ref JWT_SECRET: String = env::var("JWT_SECRET").expect("JWT_SECRET not set.");
    pub static ref DATABASE_URL: String = format!("mysql://{}:{}@{}:{}/{}",
        env::var("DB_USER").unwrap_or(String::new()),
        env::var("DB_PASSWORD").unwrap_or(String::new()),
        env::var("DB_HOST").unwrap_or(String::new()),
        env::var("DB_PORT").unwrap_or(String::new()),
        env::var("DB_NAME").unwrap_or(String::new())
    );
}
