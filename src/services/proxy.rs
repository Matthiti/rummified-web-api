use actix_web::{get, HttpRequest, HttpResponse, HttpMessage};
use actix_web::web::Path;
use actix_web::client::Client;
use actix_web::body::BodyStream;
use aes_gcm::{Key, Nonce, Aes256Gcm};
use aes_gcm::aead::{Aead, NewAead};
use aes_gcm::aes::cipher::generic_array::GenericArray;
use serde::Deserialize;

use crate::config;
use crate::errors::ServiceError;
use crate::response;
use crate::security::auth_user::AuthUser;
use actix_web::dev::Body;

#[derive(Deserialize)]
struct RummifiedErrorResponse {
    code: u16,
    errors: Vec<RummifiedErrors>
}

#[derive(Deserialize)]
struct RummifiedErrors {
    message: String,
    key: String
}

// #[route("/api/proxy/{path:.*}", method = "GET", method = "POST", method = "PUT", method = "DELETE")]
// TODO: support POST/PUT/DELETE
#[get("/api/proxy/{path:.*}")]
pub async fn proxy(req: HttpRequest, path: Path<String>) -> Result<HttpResponse, ServiceError> {
    let extensions = req.extensions();
    let auth_user: &AuthUser = extensions.get().unwrap();
    let encrypted_credentials = base64::decode(&auth_user.user.credentials).unwrap();

    let key = base64::decode(&auth_user.secret_key.key).unwrap();
    let nonce = base64::decode(&auth_user.secret_key.nonce).unwrap();


    let aes_key = Key::from(GenericArray::clone_from_slice(key.as_slice()));
    let aes_nonce = Nonce::from(GenericArray::clone_from_slice(nonce.as_slice()));
    let cipher: Aes256Gcm = Aes256Gcm::new(&aes_key);

    let decrypted_credentials = cipher.decrypt(&aes_nonce, encrypted_credentials.as_ref());
    if decrypted_credentials.is_err() {
        return Err(ServiceError::InternalServerError(String::from("An unknown error occurred")));
    }

    let credentials = String::from_utf8(decrypted_credentials.unwrap());
    if credentials.is_err() {
        return Err(ServiceError::InternalServerError(String::from("An unknown error occurred")));
    }

    let client = Client::default();

    let mut res = client.get(format!("{}/{}", *config::RUMMIFIED_API_BASE_URL, path.0))
        .header("Authorization", format!("Basic {}", credentials.unwrap()))
        .send()
        .await;

    if res.is_err() {
        return Err(ServiceError::InternalServerError(String::from("An unknown error occurred")));
    }

    let mut res = res.unwrap();
    if !res.status().is_success() {
        // TODO: pass the whole JSON response to the client?
        let error_json = res.json::<RummifiedErrorResponse>().await.unwrap();

        let mut msg = String::new();
        for error in error_json.errors {
            msg.push_str(&error.message);
        }
        return Err(ServiceError::RummifiedError(res.status(), msg))
    }

    // TODO: wrap around SuccessResponse
    Ok(HttpResponse::Ok().header("Content-Type", "application/json").streaming(res))
}
