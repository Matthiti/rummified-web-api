use actix_web::{post, web, HttpResponse};
use serde::Deserialize;
use actix_web::client::Client;
use std::collections::HashMap;
use aes_gcm::{Key, Aes256Gcm, Nonce};
use aes_gcm::aead::{Aead, NewAead};
use rand::rngs::{StdRng};
use rand::{Rng, SeedableRng};
use diesel::prelude::*;
use diesel::{insert_into, update};

use crate::{config, response, security, Pool};
use crate::db::schema::users::dsl as schema;
use crate::models::user::{User, NewUser};
use crate::errors;
use crate::errors::ServiceError;

#[derive(Deserialize)]
pub struct Credentials {
    email: String,
    password: String
}

#[derive(Deserialize)]
struct AccountResponse {
    id: u64,
    username: String,
    email: String,
    has_payed: bool,
    premium_end: String,
    new_account: bool
}

#[post("/api/auth/login")]
pub async fn login(db: web::Data<Pool>, credentials: web::Json<Credentials>) -> Result<HttpResponse, ServiceError> {
    let client = Client::default();

    let encoded_credentials = base64::encode(format!("{}:{}", credentials.email, credentials.password));
    let mut res = client.get(format!("{}/account", *config::RUMMIFIED_API_BASE_URL))
        .header("Authorization", format!("Basic {}", encoded_credentials))
        .send()
        .await;

    if res.is_err() || !res.as_ref().unwrap().status().is_success() {
        return Err(errors::ServiceError::Unauthorized(String::from("Incorrect email or password")));
    }

    let account_response = res.unwrap().json::<AccountResponse>().await.unwrap();
    let account_id = account_response.id;

    let mut prng = StdRng::from_entropy();
    let key = prng.gen::<[u8; 32]>();
    let nonce = prng.gen::<[u8; 12]>();

    let aes_key = Key::from(key.clone());
    let aes_nonce = Nonce::from(nonce.clone());
    let cipher: Aes256Gcm = Aes256Gcm::new(&aes_key);

    let encryption_result = cipher.encrypt(&aes_nonce, encoded_credentials.as_ref());
    if encryption_result.is_err() {
        return Err(ServiceError::InternalServerError(String::from("An unknown error occurred")));
    }

    let ciphertext = base64::encode(encryption_result.unwrap());

    let conn = db.get().unwrap();
    let user: User = match schema::users.filter(schema::external_id.eq(account_id)).get_result::<User>(&conn) {
        Ok(user) => { // User exists
            if update(&user).set(schema::credentials.eq(ciphertext)).execute(&conn).is_err() {
                return Err(ServiceError::InternalServerError(String::from("An unknown error occurred")));
            };
            schema::users.filter(schema::uuid.eq(user.uuid)).get_result::<User>(&conn).unwrap()
        },
        Err(_) => { // User does not exist
            let user = NewUser {
                uuid: uuid::Uuid::new_v4().to_string(),
                external_id: account_id,
                credentials: ciphertext
            };

            if insert_into(schema::users).values(&user).execute(&conn).is_err() {
                return Err(ServiceError::InternalServerError(String::from("An unknown error occurred")));
            }
            schema::users.filter(schema::uuid.eq(user.uuid)).get_result::<User>(&conn).unwrap()
        }
    };

    match security::jwt::create_token(&user.uuid, &base64::encode(key), &base64::encode(nonce)) {
        Ok(token) => {
            let mut data = HashMap::new();
            data.insert(String::from("token"), token);

            Ok(HttpResponse::Ok().json(response::SuccessResponse::new(data)))
        }
        Err(_) => Err(ServiceError::InternalServerError(String::from("Cannot generate JWT")))
    }
}
